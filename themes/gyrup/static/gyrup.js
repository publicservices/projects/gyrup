;(function() {
	var map
	var main = document.querySelector('.Layout-bottom')
	var startLocation = [56.92, 8.54]
	var locations = [
		{
			title: 'Gyrup Gårdmalteri',
			url: '/malten/',
			lat: 56.89142525277416,
			lng: 8.427865505218508
		},
		{
			title: 'Thisted Bryghus',
			url: '/bryggeren/',
			lat: 56.95315,
			lng: 8.68134
		},
		{
			title: 'Speltmark',
			url: '/raavarene#spelt/',
			polygon: [
				[56.88840117255047, 8.40533494949341],
				[56.88865904940164, 8.40660095214844],
				[56.88881143034083, 8.412158489227297],
				[56.8888348735071, 8.416192531585695],
				[56.88833084219127, 8.417179584503176],
				[56.88742825749843, 8.41780185699463],
				[56.88747514555179, 8.41681480407715],
				[56.88724070469667, 8.416686058044435],
				[56.886525651004426, 8.416707515716555],
				[56.886408427782285, 8.407158851623537],
				[56.88732275916326, 8.40507745742798],
				[56.887662697176886, 8.40535640716553],
				[56.8884011725504, 8.405334949493417]
			]
		},
		{
			title: 'Bygmark 1',
			url: '/raavarene#byg/',
			polygon: [
				[56.88338394152481, 8.412051200866701],
				[56.88307913475978, 8.414540290832521],
				[56.882868113235475, 8.414454460144045],
				[56.88272743155735, 8.415012359619142],
				[56.882422619438195, 8.414690494537355],
				[56.88229365971636, 8.415012359619142],
				[56.88224676516174, 8.415741920471193],
				[56.882879836684765, 8.416943550109865],
				[56.88107438217392, 8.420183658599855],
				[56.87885847789477, 8.418509960174562],
				[56.87923366669308, 8.41258764266968],
				[56.87905779741275, 8.4105920791626],
				[56.87950333131585, 8.410699367523195],
				[56.879855064856734, 8.410356044769289],
				[56.880113000683174, 8.409712314605715],
				[56.880347486253626, 8.409218788146974],
				[56.88076955657355, 8.408703804016115],
				[56.88107438217392, 8.407931327819826],
				[56.88134403351744, 8.40735197067261],
				[56.881590235219065, 8.407201766967775],
				[56.88147299651579, 8.409175872802736],
				[56.881637130597426, 8.409605026245119],
				[56.88278604898761, 8.411471843719484],
				[56.88312602827006, 8.41183662414551],
				[56.8833839415248, 8.4120512008667011]
			]
		},
		{
			title: 'Bygmark 2',
			url: '/raavarene#byg/',
			polygon: [
				[56.888459781082005, 8.480587005615236],
				[56.88967881769707, 8.469944000244142],
				[56.89195268356011, 8.471145629882814],
				[56.892937201106456, 8.473827838897707],
				[56.893476330674105, 8.474771976470949],
				[56.89339428972004, 8.475630283355715],
				[56.893605251809355, 8.478569984436037],
				[56.8934997709136, 8.480072021484377],
				[56.893206766862754, 8.48000764846802],
				[56.89314816577685, 8.480887413024904],
				[56.89280827766589, 8.480801582336428],
				[56.89271451488424, 8.482024669647219],
				[56.89133148652161, 8.481595516204836],
				[56.89122599920622, 8.482539653778078],
				[56.88925684798474, 8.481531143188478],
				[56.888459781082, 8.4805870056152365]
			]
		},
		{
			title: 'Hvedemark',
			url: '/raavarene#hvede/',
			polygon: [
				[56.82286628328902, 8.52895259857178],
				[56.82326551818781, 8.524532318115236],
				[56.82514421352932, 8.525133132934572],
				[56.825449492622674, 8.522343635559084],
				[56.82678799466142, 8.522386550903322],
				[56.82674103048731, 8.523759841918947],
				[56.828196892502234, 8.52444648742676],
				[56.82770378428647, 8.5259485244751],
				[56.82702281464866, 8.525862693786623],
				[56.82683495877666, 8.526635169982912],
				[56.826107008373235, 8.52719306945801],
				[56.8252146627717, 8.528265953063967],
				[56.82439274669952, 8.527793884277346],
				[56.82422836132094, 8.528609275817873],
				[56.822866283289, 8.528952598571782]
			]
		}
	]

	function createMap() {
		// Set starting poisiton.
		map = L.map('map', {
			scrollWheelZoom: false
		}).setView([56.92, 8.54], 9)

		// Choose which tile (map style) to use.
		L.tileLayer(
			'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
			{
				attribution: 'Tiles &copy; Esri'
			}
		).addTo(map)
	}

	function addMarkers(points) {
		points.forEach(function(point) {
			var marker
			if (point.polygon) {
				marker = new L.polygon(point.polygon, {point, color: '#893b44'})
			} else {
				marker = new L.Marker([point.lat, point.lng], {point})
			}
			marker
				.addTo(map)
				.bindTooltip(point.title, {opacity: 0.7})
				.openTooltip()
				.on('click', onClick)
		})
	}

	function onClick(e) {
		// console.log(e, this.options);
		map.flyTo(e.latlng)
		// Switch page
		const url = this.options.point.url
		if (url) {
			// window.location.href = this.options.url;
			console.log(url)
			Barba.Pjax.goTo(url)
		}
	}

	document.addEventListener('DOMContentLoaded', function() {
		createMap()
		addMarkers(locations)
	})

	const nextButton = document.querySelector('.UniversalNext')
	if (nextButton) {
		nextButton.addEventListener('click', goToNextPage)
	}

	function goToNextPage(e) {
		console.log('goToNextPage')
		const nav = document.querySelector('.FixedNav')

		const current = nav.querySelector('.active')

		if (!current) {
			// window.location = '/'
			Barba.Pjax.goTo('/')
			return
		} else if (current.nextElementSibling) {
			current.nextElementSibling.click()
		} else {
			// window.location = "/outro";
			Barba.Pjax.goTo('/outro')
		}
	}

	function randomIntFromInterval(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min)
	}

	function moveMap(path) {
		locations.forEach(function(loc, i) {
			if (!map) return
			// exceptions where path doesn't match
			if (path === '/outro') {
				map.flyTo(startLocation, 10)
			} else if (path === '/') {
				map.flyTo(startLocation, 9)
			} else if (path.includes('oellen')) {
				map.flyTo(startLocation, 10)
			} else if (path.includes('raavarene')) {
				var i = randomIntFromInterval(2, 4)
				map.flyTo(locations[i].polygon[0], 12)
			} else if (loc.url.includes(path)) {
				// when path does match
				map.flyTo([loc.lat, loc.lng], 16)
			}
		})
	}

	if (Barba) {
		Barba.Dispatcher.on('newPageReady', function(currentStatus) {
			// Update map if we can
			const path = currentStatus.url.split(window.location.origin)[1]
			console.log('new page ready', path)
			moveMap(path)
		})
	}
})()

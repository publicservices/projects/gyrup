---
containImage: true
title: Øllen
weight: 1
---

<div class="tc mt3">
	<img class="HeaderImage" src="/oellen.png">
</div>

# Øl Spelt

Denne blonde fra Thisted Bryghus er brygget med massere af spelt, der har været brugt til ølbrygning igennem tusinder af år. Den lyse farve og milde smag gør Øl Spelt ideel til frokostbordet.

## Fakta

Alc
: 5,0% vol

Malt
: Byg (Vienna, Cara, Biscuit), Hvede, Spelt (umaltet)

Humle
: Magnum, First Cold

Gær
: Trappist Ale

Bitterhed
: 23 IBU (Mild)

Farve
: 10 EBC (gylden)

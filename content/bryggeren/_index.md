---
description: N⁰4 Bryggeren - Antoni — Thisted Bryghus
title: Bryggeren
weight: 4
---

# Bryggeren

Antoni Aagaard Madsen kom til <a href="http://www.thistedbryghus.dk" target="_blank">Thisted Bryghus</a> med en mikrobrygger baggrund i 2012. Det er blevet et af brygmesterens særkender, at udvikle ølopskrifter baseret på lokale råvarer.

<img src="/brygger.jpg" alt="Antoni">

---
title: Malten
weight: 3
---

# Gyrup Gårdmalt

Før kornet kan blive til øl skal den maltes. Det gør Jakob og Andreas på deres malteri på gården Gyrup i Thy. Gyrup Gårdmalt laver malt i små batches i en særlig tromle, hvor kornet spirer. Når kornet spirer nedbrydes cellevægge og stivelsen gøres tilgængelig.

<img src="/malterne.jpg" alt="Malterne Andreas og Jakob">

# Fakta

De enkelte typer korn og malt i øllen bidrager med forskellige ting.

Vienna
: Den primære kilde til stivelse og sukker. Mere karakterfyldt end pilsnermalten.

Karamel
: Farver øllen gylden og giver dybde og sødme til smagen

Biscuit
: Bidrager med stor dybde i smagen og noter af friskristet brød

Spelt (umaltet)
: Giver en mild nøddeagtig smag af korn og let syrlige toner.

Hvede
: Hvedemalten giver stabilit skum og en mild og frisk smag
